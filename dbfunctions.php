<?php

    /*
     *  STATUS KEY:
     *      DEAD = no function, just prototype
     *      LIVE = works, but not in final state
     *      FIN = function, in final state
     */


    function createConnection() { // STATUS = FIN
        // Creates a connection to the db
        $servername = getenv('IP');
        $username = getenv('C9_USER');
        $password = "";
        $database = "designproject";
        $dbport = 3306;
        
        // Create connection
        $GLOBALS['db'] = new mysqli($servername, $username, $password, $database, $dbport);
        
        // Check connection
        if ($GLOBALS['db']->connect_error) {
            die("Connection failed: " . $GLOBALS['db']->connect_error);
        }
        // echo "Connected successfully (".$GLOBALS['db']->host_info.")";
        
    }
    
    function getSetting($settingName) { // STATUS = FIN
        // Returns specified setting
        $query = "select " . $settingName . " from users where username = '" . $_SESSION['username'] . "'";
        $result = mysqli_query($GLOBALS['db'], $query);
        $row = mysqli_fetch_array($result);
        $text = $row[$settingName];
        return $text;
    }
    
    function setset($field, $value) { // STATUS = FIN
        // See putSettings function for useage
        $query = "update users set " . $field . " = '" . $value . "' where 
            username = '" . $_SESSION["username"] . "'";
        $result = mysqli_query($GLOBALS['db'], $query);
        if(!$result) {
            return false;
        }
        else {
            return true;
        }
    }
    
    function putSettings($firstname, $lastname, $height, $weight, $dob) { // STATUS = FIN
        // Stores settings in db for logged in user
        $clear = false;
        $clear = setset("firstname", $firstname);
        $clear = setset("lastname", $lastname);
        $clear = setset("height", $height);
        $clear = setset("weight", $weight);
        $clear = setset("dob", $dob);
        return $clear;
    }
    
    function checkLogin($username, $password) { // STATUS = FIN
        // Returns true if login info is valid, else returns false
        $query = "select * from users where username = '" . $username . "' and password = '" . $password . "'";
        $result = mysqli_query($GLOBALS['db'], $query);
        if ($row = mysqli_fetch_assoc($result)) {
            return true;
        }
        else {
            return false;
        }
    }
    
    function addNewUser($username, $password) { // STATUS = FIN
        // Take username and password and create a new user entry in db
        // Returns true if user added successfully
        // Returns false if username already is in use
        $query = "select * from users where username = '" . $username . "'";
        $result = mysqli_query($GLOBALS['db'], $query);
        if ($row = mysqli_fetch_assoc($result)) {
            return false;
        }
        else {
            $query = "insert into users (username, password) values ('".$username."', '". $password. "')";
            $result = mysqli_query($GLOBALS['db'], $query);
            if(!$result) {
                print "Error inserting data!";
            }
            else {
                return true;
            }
        }
    }
    
    function getID ($tablename, $where, $name) { // STATUS = FIN
        $query = "select * from " . $tablename . " where " . $where . " = '" . $name . "'";
        $result = mysqli_query($GLOBALS['db'], $query);
        $row = mysqli_fetch_array($result);
        if ($tablename == "users") {
            $rowname = "userID";
        }
        else if ($tablename == "plans") {
            $rowname = "planID";
        }
        else {
            $rowname = "matchID";
        }
        $text = $row[$rowname];
        return $text;
    }
    
    function linkUserPlan($selectedPlan) { // STATUS = FIN
        // Connect in database a user with a plan
        // return a boolean success or error
        $userID = getID("users", "username", $_SESSION["username"]);
        $planID = getID("plans", "name", $selectedPlan);
        $query = "select * from userplan where userID = '" . $userID
                    . "' and planID = '" . $planID . "'";
        $result = mysqli_query($GLOBALS['db'], $query);
        if ($row = mysqli_fetch_assoc($result)) {
            return false;
        }
        else {
            $query = "insert into userplan (userID, planID) values ('".$userID."', '". $planID. "')";
            $result = mysqli_query($GLOBALS['db'], $query);
            if(!$result) {
                print "Error inserting data!";
                return false;
            }
            else {
                return true;
            }
        }
    }
    
    function unlinkUserPlan($selectedPlan) { // STATUS = FIN
        // Connect in database a user with a plan
        // return a boolean success or error
        $userID = getID("users", "username", $_SESSION["username"]);
        $planID = getID("plans", "name", $selectedPlan);
        $query = "select * from userplan where userID = '" . $userID
                    . "' and planID = '" . $planID . "'";
        $result = mysqli_query($GLOBALS['db'], $query);
        if ($row = mysqli_fetch_assoc($result)) {
            $match = $row['matchID'];
            $query = "delete from userplan where matchID=" . $match;
            $result = mysqli_query($GLOBALS['db'], $query);
            if(!$result) {
                print "Error inserting data!";
                return false;
            }
            else {
                return true;
            }
        }
        else {
            return false;
        }
    }
    
    function printUserPlans() { // STATUS = FIN
        $ids = array();
        $names = array();
        $title = array();
        $query = "select * from userplan where userID = '" . getID("users", "username", $_SESSION["username"]) . "'";
        $result = mysqli_query($GLOBALS['db'], $query);
        $i = 0;
        while ($row = mysqli_fetch_assoc($result)) {
            $ids[$i] = $row['planID'];
            $i++;
        }
        for ($j = 0 ; $j < $i ; $j++) {
            // query the plans database and add each plan
            // connected to each planID into the array 'names'
            $query = "select * from plans where planID = '" . $ids[$j] . "'";
            $result = mysqli_query($GLOBALS['db'], $query);
            $row = mysqli_fetch_assoc($result);
            $names[$j] = $row['name'];
            $title[$j] = $row['title'];
        }
        if ($i > 0) {
            for ($j = 0 ; $j < $i ; $j++) 
            {
                $link = "removeplan.php?opt=" . $names[$j];
                echo "<br><h3><a class='plan' href='" . $link . "'>" . $title[$j] . "</a></h3>";
            }
        }
        else {
            echo "<br><h3>You have no plans to remove...</h3>";
            $link = "newplan.php";
            echo "<br><h3><a class='plan' href='" . $link . "'>Click here to add some!</a></h3>";
        }
        $link = "dashboard.php";
        echo "<br><h3><a class='plan' href='" . $link . "'><< Back to Dashboard</a></h3>";
    }
    
    function printPlans() { // STATUS = LIVE
        $ids = array();
        $names = array();
        $title = array();
        $query = "select * from userplan where userID = '" . getID("users", "username", $_SESSION["username"]) . "'";
        $result = mysqli_query($GLOBALS['db'], $query);
        $i = 0;
        while ($row = mysqli_fetch_assoc($result)) {
            $ids[$i] = $row['planID'];
            $i++;
        }
        // query the plans database and add each plan
        // connected to each planID into the array 'names'
        $query = "select * from plans where planID != '" . $ids[0] . "'";
        for ($j = 1 ; $j < $i ; $j++) {
            $query = $query . " and planID != '" . $ids[$j] . "'";
        }
        $result = mysqli_query($GLOBALS['db'], $query);
        $i = 0;
        $any = false;
        while ($row = mysqli_fetch_assoc($result)) {
            $names[$i] = $row['name'];
            $title[$i] = $row['title'];
            $any = true;
            $i++;
        }
        if ($any) {
            for ($j = 0 ; $j < $i ; $j++) 
            {
                $link = "newplan.php?opt=" . $names[$j];
                echo "<br><h3><a class='plan' href='" . $link . "'>" . $title[$j] . "</a></h3>";
            }
        }
        else {
            echo "<br><h3>Hey! Looks like you have already added all our plans!<br><br>Good on ya, mate!</h3>";
        }
        $link = "dashboard.php";
        echo "<br><h3><a class='plan' href='" . $link . "'><< Back to Dashboard</a></h3>";
    }
    
    function addPlan($filename, $title) { // STATUS = FIN
        $query = "select * from plans where name = '" . $filename . "'";
        $result = mysqli_query($GLOBALS['db'], $query);
        if ($row = mysqli_fetch_assoc($result)) {
            return false;
        }
        else {
            $query = "insert into plans (name, title) values ('". $filename ."', '". $title ."')";
            $result = mysqli_query($GLOBALS['db'], $query);
            if(!$result) {
                print "Error inserting data!";
            }
            else {
                return true;
            }
        }
    }
    
    function dashShowPlans() {
        
        $ids = array();
        $names = array();
        $title = array();
        $query = "select * from userplan where userID = '" . getID("users", "username", $_SESSION["username"]) . "'";
        $result = mysqli_query($GLOBALS['db'], $query);
        $i = 0;
        while ($row = mysqli_fetch_assoc($result)) {
            $ids[$i] = $row['planID'];
            $i++;
        }
        for ($j = 0 ; $j < $i ; $j++) {
            // query the plans database and add each plan
            // connected to each planID into the array 'names'
            $query = "select * from plans where planID = '" . $ids[$j] . "'";
            $result = mysqli_query($GLOBALS['db'], $query);
            $row = mysqli_fetch_assoc($result);
            $names[$j] = $row['name'];
            $title[$j] = $row['title'];
        }
        if ($i > 0) {
            for ($j = 0 ; $j < $i ; $j++) 
            {
                $item = $names[$j];
                echo "<br><br><button id=\"expandable\" onclick='toggle_visibility(\"" . $item . "\")'>" . $title[$j] . "</button>";
                echo "<div class=\"expansion\" id=\"" . $item . "\">";
                include("plans/" . $item . ".html");
                echo "</div>";
            }
        }
        else {
            echo "<br><button id=\"expandable\">You have chosen no plans...</button>";
        }
    }
?>