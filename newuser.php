<?php
    session_start();
    include("dbfunctions.php");
    createConnection();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>New Account</title>
        <link href="style.css" type="text/css" rel="stylesheet" />
    </head>
    <body>
        <form id="newuser" class="centered" action="newuser.php" method="post" accept-charset="UTF-8">
            <fieldset>
                <legend><h1>Join</h1></legend>
                <input type="hidden" name="submitted" value="1"/>
                <br/>
                <input type="text" name="username" maxlength="50" placeholder="Username" />
                <br/><br/>
                <input type="password" name="password" maxlength="50" placeholder="Password" />
                <br/><br/>
                <input type="password" name="password_confirmation" maxlength="50" placeholder="Confirm Password" />
                <br/><br/>
                <input type="submit" class="button" name="Submit" value="Submit" />
            </fieldset>
            <h2>Already have an account?</h2>
            <h2><a href=login.php style="color: silver;">Click here!</a></h2>
        </form>
        
        <?php
            if (isset( $_POST["Submit"] )) {
                if ($_POST["username"] != "" && $_POST["password"] != "" && $_POST["password_confirmation"] != "") {
                    if ($_POST["password"] != $_POST["password_confirmation"]) {
                        echo "<div class='error'><b>Error:</b> Passwords do not match!</div>";
                    }
                    // Maybe add actual password and username checking using regex here, if we have time later
                    if (addNewUser($_POST["username"], $_POST["password"])) {
                        $_SESSION["username"] = $_POST["username"];
                        header("Location: settings.php");
                    }
                    else {
                        echo "<div class='error'><b>Error:</b> This username has already been claimed!</div>";
                    }
                }
                else {
                    echo "<div class='error'><b>Error:</b> Must fill all fields!</div>";
                }
            }
        ?>
    </body>
</html>