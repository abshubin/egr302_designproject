<?php
    session_start();
    include("dbfunctions.php");
    if (is_null($_SESSION["username"])) {
        header("Location: login.php");
    }
    createConnection();
?>
<!DOCTYPE html>
<html>
    <head> 
        <title>Dashboard: <?= $_SESSION["username"]; ?></title>
        <link rel="stylesheet" type="text/css" href="style.css">
        <script src="functions.js" type="text/javascript"></script>
    </head>
    
<div class = "nav">
    <ul>
        <li><a href ="index.html"><img src ="http://www.brandcrowd.com/gallery/brands/pictures/picture12558924818681.jpg" alt =""/></a></li>
        <li><a href="settings.php"><p> Settings</p></a></li>
        <li><a href="login.php?ses=exp"><p> Logout</p></a></li>
    </ul>
</div>
    
        <div id = "title">
                            <div id = "titleOfPage">
                            <h1>My Active Plans</h1> 
                            </div>
        </div>
     </br>
     
    <!--PRINT OUT ALL USER PLANS IN THEIR ENTIRETY-->
    <div id="dashPlanList" class="centered">
        <fieldset>
            <?php dashShowPlans(); ?>
        </fieldset>
        <br>
    </div>
     
     
     </br>
    <body id="bodyBackground">
        
        <!-- ****************** Buttons ************** -->
        
        <div class="centered bottomBuffered">
                <a class="button" id="loginButton" href="newplan.php">Add A Plan</a>
                <a class="button" id="newUserButton" href="removeplan.php">Remove A Plan</a>
            </div>
        
        <!-- ******Display List of what plans they have ****** --> 
        
        
        
         
         
         
        <!-- **************** Footer ***************** -->
       <div class = "footer">
        <footer>
            <p>Follow us on social Media!
            
            <img id = "facebook" src = "https://pbs.twimg.com/profile_images/3513354941/24aaffa670e634a7da9a087bfa83abe6_200x200.png"/>
            <img id = "twitter" src = "http://resources.razorplanet.com/510611-8783/510611_1994_607449.jpg"/>
            <img id = "instagram" src = "http://3835642c2693476aa717-d4b78efce91b9730bcca725cf9bb0b37.r51.cf1.rackcdn.com/Multi-Color_Logo_thumbnail200.png"/>
               
            </p>
        </footer>
        </div>
</html>


