<?php
    session_start();
    include("dbfunctions.php");
    if (is_null($_SESSION["username"])) {
        header("Location: login.php");
    }
    createConnection();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Settings</title>
        <link href="style.css" type="text/css" rel="stylesheet" />
    </head>
    <body>
        <form id="settings" class="centered" action="settings.php" method="post" accept-charset="UTF-8">
            <fieldset>
                <legend><h1>Info</h1></legend>
                <input type="hidden" name="submitted" value="1"/>
                <br/>
                <input type="text" name="firstname" maxlength="100" placeholder="First Name" value=<?= getSetting("firstname") ?> >
                <br/><br/>
                <input type="text" name="lastname" maxlength="100" placeholder="Last Name" value=<?= getSetting("lastname") ?> >
                <br/><br/>
                <span>Height:<br/><input type="number" name="height" min="20" max="100" value=<?= getSetting("height") ?> >in.</span>
                <br/><br/>
                <span>Weight:<br/><input type="number" name="weight" min="75" max="500" value=<?= getSetting("weight") ?> >lbs.</span>
                <br/><br/>
                <span>Born:<br/><input type="date" name="birth" value=<?= getSetting("dob") ?> /></span>
                <br/><br/>
                <input type="submit" class="button" name="Submit" value="Submit" />
            </fieldset>
        </form>
        
        <?php
            $cleared = true;
        
            if (!($_POST["height"] >= 20 && $_POST["height"] <= 100) && $_POST["height"] != "") {
                echo "<div class='error'><b>Error:</b> Height must be between 20 and 100 inches!</div>";
            }
            if (!($_POST["weight"] >= 75 && $_POST["weight"] <= 500) && $_POST["weight"] != "") {
                echo "<div class='error'><b>Error:</b> Weight must be between 75 and 500 pounds!</div>";
            }
            
        
            if ($_POST["firstname"] == "" && $_POST["lastname"] == "" && $_POST["height"] == ""
                    && $_POST["weight"] == "" && $_POST["birth"] == ""){
                /* do nothing */
            }
            else if ($cleared && ($_POST["firstname"] != "" && $_POST["lastname"] != "" && $_POST["height"] != ""
                    && $_POST["weight"] != "" && $_POST["birth"] != "")) {
                $cleared = putSettings($_POST["firstname"], $_POST["lastname"], $_POST["height"], $_POST["weight"], $_POST["birth"]);
                if ($cleared) {
                    header("Location: dashboard.php");
                }
                else {
                    echo "<div class='error'><b>Error:</b> Error updating data!</div>";
                }
            }
            else {
                echo "<div class='error'><b>Error:</b> Must fill all fields!</div>";
            }
        ?>
    </body>
</html>