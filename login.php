<?php
    session_start();
    include("dbfunctions.php");
    createConnection();
    if ($_GET["ses"] == "exp") {
        session_unset();
        session_destroy();
    }
    if (isset( $_POST["Submit"] )) {
        if (checkLogin($_POST["username"], $_POST["password"])) {
            $_SESSION["username"] = $_POST["username"];
            header("Location: dashboard.php");
        }
        else {
            echo "<div class='error'><b>Error:</b> Incorrect login information entered!</div>";
        }
    }
    if ($_SESSION["username"] == "admin") {
        header("Location: admindash.php");
    }
    else if (!is_null($_SESSION["username"])) {
        header("Location: dashboard.php");
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Log In</title>
        <link href="style.css" type="text/css" rel="stylesheet" />
    </head>
    <body>
        <form id="login" class="centered" action="login.php" method="post" accept-charset="UTF-8">
            <fieldset >
                <legend><h1>Login</h1></legend>
                <input type="hidden" name="submitted" value="1"/>
                <br/>
                <input type="text" name="username" maxlength="50" placeholder="Username" />
                <br/><br/>
                <input type="password" name="password" maxlength="50" placeholder="Password" />
                <br/><br/>
                <input type="submit" class="button" name="Submit" value="Submit" />
            </fieldset>
            <h2>Don't have an account?</h2>
            <h2><a href=newuser.php style="color: silver;">Click here!</a></h2>
        </form>
    </body>
</html>