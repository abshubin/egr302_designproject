<?php
    session_start();
    include("dbfunctions.php");
    if (is_null($_SESSION["username"])) {
        header("Location: login.php");
    }
    createConnection();
    
    $plan = $_GET["opt"];
    $action = $_GET["act"];
    $error = "";
    if ($action == "log") {
        $visibility1 = $visibility2 = "hidden";
        $success = linkUserPlan($_SESSION["plan"]);
        if ($success) {
            header("Location: dashboard.php");
        }
        else {
            $error = "<div class='error'><b>Error:</b> Problem with submission!</div>";
            $visibility1 = "";
        }
    }
    else if (isset($_GET["opt"])) {
        // alter the page so that it shows a preview of the plan,
        // and allows the user to either confirm or deny the selection.
        $visibility1 = "hidden";
        $visibility2 = "";
        $_SESSION["plan"] = $plan;
    }
    else {
        $visibility1 = "";
        $visibility2 = "hidden";
        $_SESSION["plan"] = "";
    }
    
?>

<!DOCTYPE html>
<html>
    <head>
        <title>New Plan</title>
        <link href="style.css" type="text/css" rel="stylesheet" />
    </head>
    <body>
        
        <form <?= $visibility1?> id="newplan" class="centered" action="newplan.php" method="post" accept-charset="UTF-8">
            <fieldset>
                <legend class="right"><h1>Choose </h1></legend>
                <?php printPlans(); ?>
            </fieldset>
        </form>
        
        <form <?= $visibility2?> id="newplan" class="centered" action="newplan.php" method="post" accept-charset="UTF-8">
            <fieldset>
                <legend class="right"><h1>Confirm </h1></legend>
                <!-- print selected plan with confirm/deny buttons -->
                <p><?php include("plans/" . $_SESSION["plan"] . ".html") ?></p>
                <br>
                <button type="submit" class="button" formaction="newplan.php?act=log">Confirm</button>
                <button type="submit" class="button" formaction="newplan.php">Cancel</button>
            </fieldset>
        </form>
        
        <?= $error ?>

    </body>
</html>