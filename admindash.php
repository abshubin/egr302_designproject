<?php
    session_start();
    include("dbfunctions.php");
    if ($_SESSION["username"] != "admin") {
        header("Location: login.php?ses=exp");
    }
    createConnection();
    if (!isset($_SESSION["rows"])) {
        $_SESSION["rows"] = 1;
    }
    if (isset($_POST["Add"])) {
        $_SESSION["rows"]++;
    }
    if (isset($_POST["Rem"])) {
        $_SESSION["rows"]--;
        if ($_SESSION["rows"] < 0) {
            $_SESSION["rows"] = 0;
        }
    }
    
    function getPre($fieldName) {
        if (isset($_POST[$fieldName])) {
            return $_POST[$fieldName];
        }
        else {
            return "";
        }
    }
    
    function generateRows() {
        for ($x = 0 ; $x < $_SESSION["rows"] ; $x++) {
            echo '<input type="text" name="row' . $x . '" maxlength="100" 
                    placeholder="Exercise ' . ($x + 1) . '" value="' . getPre("row" . $x) . '" ><br/><br/>';
        }
        echo '<input type="submit" class="button" name="Add" value="Add Row" > <br/><br/>';
        echo '<input type="submit" class="button" name="Rem" value="Remove Row" > <br/><br/>';
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Admin Dashboard</title>
        <script src="functions.js" type="text/javascript"></script>
    </head>
    <body>
        <a href="login.php?ses=exp">LOGOUT</a>
        <form id="addplan" action="admindash.php" method="post" accept-charset="UTF-8">
            <fieldset>
                <legend><h1>Register Plan HTML File</h1></legend>
                <input type="hidden" name="submitted" value="1"/>
                <br/>
                <input type="text" name="filename" maxlength="50" 
                        placeholder="File Name" value='<?= getPre("filename"); ?>' >
                <br/><br/>
                <input type="text" name="label" maxlength="50" 
                        placeholder="Plan Label" value='<?= getPre("label"); ?>' >
                <br/><br/>
                <input type="text" name="title" maxlength="100" 
                        placeholder="Plan Title" value='<?= getPre("title"); ?>' >
                <br/><br/>
                <input type="text" name="subtitle" maxlength="100" 
                        placeholder="Plan Subtitle" value='<?= getPre("subtitle"); ?>' >
                <br/><br/>
                <?php generateRows(); ?>
                <input type="submit" class="button" name="Submit" value="Submit" />
            </fieldset>
        </form>
    </body>
</html>

<?php
    if (isset($_GET["status"])) {
        echo "PLAN ADDED SUCCESSFULLY";
    }
    if (isset( $_POST["Submit"] )) {
        if ($_POST["filename"] != "" && $_POST["label"] != ""
                && $_POST["title"] != "" && $_POST["subtitle"] != "") {
            $rows = array();
            if (!isset($_POST["row0"])) {
                $rows[0] = "";
            }
            else {
                $x = 0;
                while (isset($_POST["row" . $x])) {
                    $rows[$x] = $_POST["row" . $x];
                    $x++;
                }
            }
            printHTML($_POST["filename"], $_POST["title"], $_POST["subtitle"], $rows);
            if (addPlan($_POST["filename"], $_POST["label"])) {
                $_SESSION["rows"] = 1;
                header("Location: admindash.php?status=true");
            }
            else {
                echo "<br><b>Error:</b> Plan already registered!<br>";
            }
        }
        else {
            echo "<br><b>Error:</b> Must fill all fields!<br>";
        }
    }
    
    function printHTML($filename, $title, $subtitle, $exercises) {
        // create and html doc that is the workout plan
        // print it into the "plans" folder
        $_SESSION["newPlan"] = fopen("plans/" . $filename . ".html", "w");
        write("<!DOCTYPE html>\n");
        write("<html>\n");
        write("\t<body>\n");
        write("\t\t<h3>" . $title . "</h3>\n");
        write("\t\t<em>(" . $subtitle . ")</em>\n");
        write("\t\t<ul>\n");
        for ($x = 0 ; $x < count($exercises) ; $x++) {
            write("\t\t\t<li>" . $exercises[$x] . "</li>\n");
        }
        write("\t\t</ul>\n");
        write("\t</body>\n");
        write("</html>");
        fclose($_SESSION["newPlan"]);
        $_SESSION["newPlan"] = null;
    }
    
    function write($txt) {
        if (isset($_SESSION["newPlan"])) {
            fwrite($_SESSION["newPlan"], $txt);
        }
    }
?>